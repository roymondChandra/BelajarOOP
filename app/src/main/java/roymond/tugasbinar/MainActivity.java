package roymond.tugasbinar;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    Hewan kucing = new Hewan("Kucing",4,"Paru-paru");
    Hewan ayam = new Hewan("Ayam", 2, "Paru-paru");
    Hewan ikan = new Hewan("Ikan", 0, "Insang");
    kucing.info();
    ayam.info();
    ikan.info();

    public static void Main(String[] args){
        new Main();
    }

}
