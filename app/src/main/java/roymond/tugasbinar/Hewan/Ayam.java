package roymond.tugasbinar.Hewan;

public class Ayam extends Hewan {
    private String alatNapas;

    public Ayam(int kaki, String nama){
        super.kaki =kaki;
        super.nama = nama;
    }

    public String getAlatNapas() {
        return alatNapas;
    }
    public void setAlatNapas(String alatNapas){
        this.alatNapas = alatNapas;
    }

    @Override
    public void info() {
        super.info();
        System.out.println("Bernafas dengan "+ alatNapas);
        System.out.println();
    }
}
