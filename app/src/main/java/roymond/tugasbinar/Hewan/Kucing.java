package roymond.tugasbinar.Hewan;

public class Kucing extends Hewan {
    private String alatNapas;

    public Kucing(int kaki, String nama){
        super.kaki = kaki;
        super.nama = nama;
    }

    public String getAlatNapas() {
        return alatNapas;
    }

    public void setAlatNapas(String alatNapas) {
        this.alatNapas = alatNapas;
    }

    @Override
    public void info() {
        super.info();
        System.out.println("Bernafas dengan "+ alatNapas);
        System.out.println();
    }
}
