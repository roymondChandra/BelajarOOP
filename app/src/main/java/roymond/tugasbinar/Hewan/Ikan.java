package roymond.tugasbinar.Hewan;

public class Ikan extends  Hewan{
    private String alatNapas;
    private String habitat;

    public Ikan(int kaki, String nama){
        super.kaki = kaki;
        super.nama = nama;
    }

    public String getAlatNapas() {
        return alatNapas;
    }

    public void setAlatNapas(String alatNapas) {
        this.alatNapas = alatNapas;
    }

    public String getHabitat() {
        return habitat;
    }

    public void setHabitat(String habitat) {
        this.habitat = habitat;
    }
    public void info(){
        super.info();
        System.out.println("Bernafas dengan "+alatNapas);
        System.out.println("Habitat :"+habitat);
        System.out.println();
    }
}
