package roymond.tugasbinar.Main;

import roymond.tugasbinar.Hewan.Ayam;
import roymond.tugasbinar.Hewan.Ikan;
import roymond.tugasbinar.Hewan.Kucing;

public class Main {
    public static void main(String[]args){
        Kucing k = new Kucing(4,"Kucing");
        Ikan i = new Ikan(0,"Ikan");
        Ayam a = new Ayam(2, "Ayam");

        k.setAlatNapas("Paru-paru");
        k.info();

        i.setKaki(0);
        i.setAlatNapas("Insang");
        i.setHabitat("Laut / Air tawar");
        i.info();

        a.setAlatNapas("Paru-paru");
        a.info();
    }
}
